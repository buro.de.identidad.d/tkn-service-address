package com.teknei.bid.command.impl.address;

import com.teknei.bid.command.Command;
import com.teknei.bid.command.CommandRequest;
import com.teknei.bid.command.CommandResponse;
import com.teknei.bid.command.Status;
import com.teknei.bid.persistence.entities.*;
import com.teknei.bid.persistence.repository.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

/**
 * Main command, executes the flow
 */
@Component
public class AddressCommand implements Command {

    @Autowired
    @Qualifier(value = "parseAddressVisionCommand")
    private Command parseAddressCommand;
    @Autowired
    @Qualifier(value = "parseAddressProxyCommand")
    private Command parseAddressCommandProxy;
    @Autowired
    @Qualifier(value = "parseAddressCommand")
    private Command parseAddressCommandKofax;
    @Autowired
    @Qualifier(value = "persistAddressCommand")
    private Command persistAddressCommand;
    @Autowired
    @Qualifier(value = "storeTasAddressCommand")
    private Command storeTasAddressCommand;
    @Autowired
    private BidScanRepository bidScanRepository;
    @Autowired
    private BidTasRepository bidTasRepository;
    @Autowired
    private BidDireNestRepository direNestRepository;
    @Autowired
    @Qualifier(value = "statusCommand")
    private Command statusCommand;
    @Autowired
    private BidClieRegEstaRepository regEstaRepository;
    @Value("${tkn.address.ocr.active}")
    private Boolean activeOcr;
    @Value("${tkn.address.tas.active}")
    private Boolean activeTas;
    @Value("${tkn.address.persist.active}")
    private Boolean activePersistence;
    @Value("${tkn.address.ocr.provider}")
    private String ocrProvider;
    @Autowired
    private BidEstaProcRepository bidEstaProcRepository;
    @Autowired
    private BidClieRegEstaRepository bidClieRegEstaRepository;
    @Autowired
    private BidDireRepository bidDireRepository;
    private static final String ESTA_PROC = "CAP-COMDO";
    private static final Logger log = LoggerFactory.getLogger(AddressCommand.class);


    @Override
    public CommandResponse execute(CommandRequest request) 
    {
    	log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".execute ");
        String scanId = findScanId(request.getId());
        String documentId = findDocumentId(request.getId());
        request.setScanId(scanId);
        request.setDocumentId(documentId);
        CommandResponse response = new CommandResponse();
        response.setId(request.getId());
        response.setScanId(request.getScanId());
        response.setDocumentId(request.getDocumentId());
/*
        if(activeOcr)
        {
            if(ocrProvider.toUpperCase().contains("KOFAX")){
                response = parseAddressCommandKofax.execute(request);
            }else if(ocrProvider.toUpperCase().contains("PROXY")){
                response = parseAddressCommandProxy.execute(request);
            }else{
                response = parseAddressCommand.execute(request);
            }
            if (!response.getStatus().equals(Status.ADDRESS_KOFAX_OK)) {
                response.setStatus(Status.ADDRESS_ERROR);
                response.setDesc(String.valueOf(response.getStatus()));
                saveStatus(request.getId(), Status.ADDRESS_KOFAX_ERROR, request.getUsername());
                return response;
            }
            if (response.getDesc() == null || response.getDesc().trim().isEmpty()) {
                response.setDesc(String.valueOf(response.getStatus()));
                response.setStatus(Status.ADDRESS_ERROR);
                saveStatus(request.getId(), Status.ADDRESS_KOFAX_ERROR, request.getUsername());
                return response;
            }
            saveStatus(request.getId(), Status.ADDRESS_KOFAX_OK, request.getUsername());
        }else
*/
        if(true)
        {
        	log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".Se manda a Adress Por defaultr ");
            saveStatus(request.getId(), Status.ADDRESS_KOFAX_OK, request.getUsername());
            response.setStatus(Status.ADDRESS_OK);
            response.setDesc(response.getDesc());
        }
        if(request.getId() == 0){
            response.setStatus(Status.ADDRESS_OK);
            response.setDesc(response.getDesc());
            return response;

        }
        if(activeTas){
            CommandRequest tasRequest = new CommandRequest();
            tasRequest.setId(request.getId());
            tasRequest.setDocumentId(documentId);
            tasRequest.setScanId(scanId);
            tasRequest.setFileContent(request.getFileContent());
            CommandResponse tasResponse = storeTasAddressCommand.execute(tasRequest);
            if (!tasResponse.getStatus().equals(Status.ADDRESS_TAS_OK)) {
                tasResponse.setStatus(Status.ADDRESS_ERROR);
                tasResponse.setDesc(String.valueOf(tasResponse.getStatus()));
                saveStatus(request.getId(), Status.ADDRESS_TAS_ERROR, request.getUsername());
                return response;
            }
            saveStatus(request.getId(), Status.ADDRESS_TAS_OK, request.getUsername());
        }else{
            saveStatus(request.getId(), Status.ADDRESS_TAS_OK, request.getUsername());
            response.setStatus(Status.ADDRESS_OK);
            response.setDesc(response.getDesc());
        }
        if(activePersistence){
            CommandRequest dbRequest = new CommandRequest();
            dbRequest.setDocumentId(request.getDocumentId());
            if(activeOcr){
                dbRequest.setData(response.getDesc());
            }else{
                dbRequest.setData("OCR-NA");
            }
            dbRequest.setScanId(request.getScanId());
            dbRequest.setId(request.getId());
            dbRequest.setUsername(request.getUsername());
            CommandResponse dbResponse = persistAddressCommand.execute(dbRequest);//TODO aqui usr crea null
            if (!dbResponse.getStatus().equals(Status.ADDRESS_DB_OK)) {
                dbResponse.setDesc(String.valueOf(dbResponse.getStatus()));
                dbResponse.setStatus(Status.ADDRESS_ERROR);
                saveStatus(request.getId(), Status.ADDRESS_DB_STATUS_ERROR, request.getUsername());
            }
            saveStatus(request.getId(), Status.ADDRESS_DB_OK, request.getUsername());
            saveStatus(request.getId(), Status.ADDRESS_OK, request.getUsername());
            response.setStatus(Status.ADDRESS_OK);
            response.setDesc(response.getDesc());
            updateStatus(request.getId());
        }else{
            updateStatus(request.getId());
            response.setStatus(Status.ADDRESS_OK);
            response.setDesc(response.getDesc());
        }
        if(!activeOcr){
            BidClieDireNestPK nestPK = new BidClieDireNestPK();
            nestPK.setIdDire(request.getId());
            nestPK.setIdClie(request.getId());
            BidClieDireNest direNest = direNestRepository.findOne(nestPK);
            if(direNest != null){
                response.setDesc(direNest.getDirNestUno());
            }else{
                BidClieDire dire = bidDireRepository.findFirstByIdClieAndIdEsta(request.getId(), 1);
                if(dire == null){
                    response.setDesc("");
                }else{
                    response.setDesc(dire.getCall());
                }
            }
        }
        return response;
    }

    /**
     * Persists the current status for the main process
     *
     * @param id
     * @param status
     * @return
     */
    private CommandResponse saveStatus(Long id, Status status, String username) 
    {
    	log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".saveStatus ");
        CommandRequest request = new CommandRequest();
        request.setId(id);
        request.setRequestStatus(status);
        request.setUsername(username);
        CommandResponse response = statusCommand.execute(request);
        return response;
    }

    /**
     * Finds the scan-id value from the db
     *
     * @param id the identifier of the record
     * @return the scan-id value from db
     */
    private String findScanId(Long id) 
    {
    	log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".findScanId ");
        try {
            BidScan responseScan = bidScanRepository.findByIdRegi(id);
            if (responseScan == null) {
                return null;
            }
            return responseScan.getScanId();
        } catch (Exception e) {
            log.error("Error find scanId: {}", e.getMessage());
        }
        return null;
    }

    /**
     * Finds the tas-id value from db
     *
     * @param id the identifier of the record
     * @return the tas-id value from db
     */
    private String findDocumentId(Long id) 
    {
    	log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".findDocumentId ");
        try {
            BidClieTas responseScan = bidTasRepository.findByIdClie(id);
            if (responseScan == null) {
                return null;
            }
            return responseScan.getIdTas();
        } catch (Exception e) {
            log.error("Error find scanId: {}", e.getMessage());
        }
        return null;
    }

    private void updateStatus(Long idClient) 
    {
    	log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".updateStatus ");
        try {
            BidEstaProc estaProc = bidEstaProcRepository.findTopByCodEstaProcAndIdEsta(ESTA_PROC, 1);
            BidClieRegEsta regEsta = bidClieRegEstaRepository.findByIdClieAndIdEstaProc(idClient, estaProc.getIdEstaProc());
            if (regEsta == null) {
                log.warn("Status for process: {} found null", idClient);
            }
            regEsta.setEstaConf(true);
            regEsta.setFchModi(new Timestamp(System.currentTimeMillis()));
            regEsta.setUsrModi("client-api");
            bidClieRegEstaRepository.save(regEsta);
        } catch (Exception e) {
            log.error("Error finding status of process for customer: {} with message: {}", idClient, e.getMessage());
        }
    }
}

package com.teknei.bid.util;

import com.teknei.bid.dto.AddressDetailDTO;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utilities for parsing address formats
 */
public class DireParseUtil {

    private static final Logger log = LoggerFactory.getLogger(DireParseUtil.class);

    public static AddressDetailDTO parseDetail(String address) 
    {
    	log.info ("lblancas:: [tkn-service-address] :: DireParseUtil.parseDetail ");
        try {
            // Si la dirección es nula, no puede realizarse la extracción de datos
            if (address == null) {
                return new AddressDetailDTO();
            }
            // Verifica si el comprobante de domicilio es un recibo de CFE (por mejorar para más tipos de recibo)
            JSONObject json = new JSONObject(address);
            if (json.optString("CFE", null) == null) {
                return null;
            }
            // Extracción de propiedades de comprobante
            if(!json.has("Direccion")){
                json.put("Direccion", "NA-NA-NA-NA-NA-NA");
            }
            String[] list = json.getString("Direccion").split("-");
            String[] properties = {"street", "suburb", "municipio", "zipCode", "locality", "state"};
            String tmp = null;
            for (int i = 0; i < list.length; i++) {
                tmp = (list[i].trim().startsWith("CP:")) ? list[i].replaceAll("([^0-9])", "") : list[i].trim();
                if (i < properties.length) {
                    json.put(properties[i], tmp);
                }
            }
            // Asignación de propiedades extraídas hacia objeto
            AddressDetailDTO dto = new AddressDetailDTO();
            if (json.has("street")) {
                dto.setStreet(json.getString("street"));
            }
            if (json.has("suburb")) {
                dto.setSuburb(json.getString("suburb"));
            }
            if (json.has("municipio")) {
                dto.setMunicipio(json.getString("municipio"));
            }
            if (json.has("zipCode")) {
                dto.setZipCode(json.getString("zipCode"));
            }
            if (json.has("locality")) {
                dto.setLocality(json.getString("locality"));
            }
            if (json.has("state")) {
                dto.setState(json.getString("state"));
            }
            //Gson gson = new Gson();
            //dto = gson.fromJson(json.toString(), AddressDetailDTO.class);
            return dto;
        } catch (JSONException | NullPointerException e) {
            log.error("Error parsing detail for address: {} with message: {}", address, e.getMessage());
            return new AddressDetailDTO();
        }
    }

}

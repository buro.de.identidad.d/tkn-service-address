package com.teknei.bid.controller.rest;

import com.teknei.bid.command.Command;
import com.teknei.bid.command.CommandRequest;
import com.teknei.bid.command.CommandResponse;
import com.teknei.bid.command.Status;
import com.teknei.bid.controller.rest.crypto.Decrypt;
import com.teknei.bid.dto.AddressDetailDTO;
import com.teknei.bid.dto.DocumentPictureRequestDTO;
import com.teknei.bid.dto.RequestEncFilesDTO;
import com.teknei.bid.persistence.entities.*;
import com.teknei.bid.persistence.repository.BidClieDireContRepository;
import com.teknei.bid.persistence.repository.BidDireNestRepository;
import com.teknei.bid.persistence.repository.BidDireRepository;
import com.teknei.bid.service.AddressService;
import com.teknei.bid.util.DireParseUtil;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.sql.Timestamp;
import java.util.Arrays;

/**
 * Rest controller. Addresses operations
 */
@RestController
@RequestMapping(value = "/address")
public class AddressController {

    @Autowired
    @Qualifier(value = "addressCommand")
    private Command command;
    @Autowired
    private BidDireNestRepository direNestRepository;
    @Autowired
    private BidClieDireContRepository direContRepository;
    @Autowired
    private AddressService addressService;
    @Autowired
    private BidDireRepository bidDireRepository;
    @Autowired
    private Decrypt decrypt;
    private static final Logger log = LoggerFactory.getLogger(AddressController.class);

    /**
     * Updates manually the address provided
     *
     * @param addressDetailDTO - The full detail of the address
     * @param id               - The identification of the main record
     * @param type             - The type of invokation. 1 for NO changes, 2 for changes
     * @return - The detail updated
     */
    @ApiOperation(value = "Updates manually the address retrieved by the customer", response = AddressDetailDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Address found, response correct"),
            @ApiResponse(code = 404, message = "Address not found")
    })
    @RequestMapping(value = "/updateManually/{id}/{type}", method = RequestMethod.POST)
    public ResponseEntity<AddressDetailDTO> updateManually(@RequestBody AddressDetailDTO addressDetailDTO, @PathVariable Long id, @PathVariable Integer type) {
    	log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".updateManually ");
        if (type == 1) {
            return new ResponseEntity<>(addressDetailDTO, HttpStatus.OK);
        } else {
            try {
                updateDire(addressDetailDTO, id);
                return new ResponseEntity<>(addressDetailDTO, HttpStatus.OK);
            } catch (Exception e) {
                log.error("Error updating address for: {} , {} with message: {}", addressDetailDTO, id, e.getMessage());
                return new ResponseEntity<>((AddressDetailDTO) null, HttpStatus.UNPROCESSABLE_ENTITY);
            }
        }
    }

    /**
     * Finds the detail address (well parsed) based on db identifier
     *
     * @param id - the identifier of the record
     * @return - The address detail
     */
    @ApiOperation(value = "Finds the detail of the address related record id", notes = "The id provided must be the related one to the customer record", response = AddressDetailDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Address found, response correct"),
            @ApiResponse(code = 404, message = "Address not found"),
            @ApiResponse(code = 500, message = "Error from server")})
    @RequestMapping(value = "/findDetail/{id}", method = RequestMethod.GET)
    public ResponseEntity<AddressDetailDTO> findDetailById(@PathVariable Long id) {
    	log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".findDetailById ");
        String direNest = getDireUno(id);
        if (direNest == null) {
            addressService.syncWithPreloaded(id, "tkn-api");
            AddressDetailDTO addressDetailDTO = getDetailPreloaded(id);
            if (addressDetailDTO == null) {
                log.error("Address could not be located for id: {}", id);
                return new ResponseEntity<>((AddressDetailDTO) null, HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(addressDetailDTO, HttpStatus.OK);
        }
        AddressDetailDTO detailDTO = DireParseUtil.parseDetail(direNest);
        if (detailDTO == null) {
            detailDTO = getDetailPreloaded(id);
            if (detailDTO == null) {
                return new ResponseEntity<>((AddressDetailDTO) null, HttpStatus.UNPROCESSABLE_ENTITY);
            }
        }
        return new ResponseEntity<>(detailDTO, HttpStatus.OK);

    }

    /**
     * Finds the address related to the db identifier
     *
     * @param id - the db identifier
     * @return - The address as a string not formatted
     */
    @ApiOperation(value = "Finds the full address of the related record", notes = "The id provided must be the related one to the customer record", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Address found, response correct"),
            @ApiResponse(code = 404, message = "Address not found"),
            @ApiResponse(code = 500, message = "Error from server")})
    @RequestMapping(value = "/find/{id}", method = RequestMethod.GET)
    public ResponseEntity<String> findById(@PathVariable Long id) {
    	log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".findById ");
        BidClieDireNestPK pk = new BidClieDireNestPK();
        pk.setIdClie(id);
        pk.setIdDire(id);
        try {
            BidClieDireNest direNest = direNestRepository.findOne(pk);
            if (direNest == null) {
                addressService.syncWithPreloaded(id, "tkn-api");
                direNest = direNestRepository.findOne(pk);
                if (direNest == null) {
                    log.error("Address could not be located for id: {}", id);
                    return new ResponseEntity<>("", HttpStatus.NOT_FOUND);
                }
                return new ResponseEntity<>(direNest.getDirNestDos(), HttpStatus.OK);
            }
            return new ResponseEntity<>(direNest.getDirNestDos(), HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error finding Address detail: {}", e.getMessage());
            return new ResponseEntity<>("Error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Uploads an address image to the system
     *
     * @param file - The address file
     * @param id   - The db identifier related to the record
     * @return - The address obtained from the attachment
     */
    @ApiOperation(value = "Uploads a document referent to the customer address", notes = "The attachment must be named 'file'", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Address received successfully, must check the returned answer string in order to determinate if this is a valid response for the business"),
            @ApiResponse(code = 422, message = "The address document could not be parsed")
    })
    @Deprecated
    @RequestMapping(value = "/upload/{id}", method = RequestMethod.POST, consumes = "multipart/form-data")
    public ResponseEntity<String> addAddressDocument(@RequestPart("file") MultipartFile file, @PathVariable Long id) {
    	log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".addAddressDocument ");
        CommandRequest request = new CommandRequest();
        request.setFiles(Arrays.asList(file));
        request.setId(id);
        request.setUsername("NA");
        CommandResponse response = command.execute(request);
        if (response.getStatus().equals(Status.ADDRESS_OK)) {
            return new ResponseEntity<>(response.getDesc(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(String.valueOf(response.getStatus().getValue()), HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Process a document referent to the customer address, source is B64 encoded and ciphered")
    @RequestMapping(value = "/uploadPlain", method = RequestMethod.POST)
    public ResponseEntity<String> uploadAddressDocument(@RequestBody RequestEncFilesDTO dto) {
    	log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".uploadAddressDocument ");
        CommandRequest request = new CommandRequest();
        request.setFileContent(Arrays.asList(Base64Utils.decodeFromString(dto.getB64Anverse())));
        request.setId(dto.getOperationId());
        request.setUsername(dto.getUsername());
        CommandResponse response = command.execute(request);
        if (response.getStatus().equals(Status.ADDRESS_OK)) {
            return new ResponseEntity<>(response.getDesc(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(String.valueOf(response.getStatus().getValue()), HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @RequestMapping(value = "/uploadPlainParse", method = RequestMethod.POST)
    public ResponseEntity<String> addAddressDocumentParsePlain(@RequestBody RequestEncFilesDTO dto) {
    	log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".addAddressDocumentParsePlain ");
        ResponseEntity<String> responseEntity = uploadAddressDocument(dto);
        if (responseEntity.getStatusCode() == HttpStatus.UNPROCESSABLE_ENTITY) {
            return responseEntity;
        } else {
            String jsonResponse = responseEntity.getBody();
            JSONObject jsonObject = new JSONObject(jsonResponse);
            AddressDetailDTO addressDetailDTO = getDetailPreloaded(dto.getOperationId());
            if (addressDetailDTO != null) {

            } else {
                String direUno = getDireUno(dto.getOperationId());
                addressDetailDTO = DireParseUtil.parseDetail(direUno);

            }
            if (addressDetailDTO == null) {
                addressDetailDTO = new AddressDetailDTO();
            }
            jsonObject.put("country", addressDetailDTO.getCountry());
            jsonObject.put("extNumber", addressDetailDTO.getExtNumber());
            jsonObject.put("intNumber", addressDetailDTO.getIntNumber());
            jsonObject.put("locality", addressDetailDTO.getLocality());
            jsonObject.put("municipio", addressDetailDTO.getMunicipio());
            jsonObject.put("state", addressDetailDTO.getState());
            jsonObject.put("stret", addressDetailDTO.getStreet());
            jsonObject.put("suburb", addressDetailDTO.getSuburb());
            jsonObject.put("zipCode", addressDetailDTO.getZipCode());
            return new ResponseEntity<>(jsonObject.toString(), HttpStatus.OK);
        }
    }

    /**
     * Uploads an address image to the system
     *
     * @param file - The address file
     * @param id   - The db identifier related to the record
     * @return - The address obtained from the attachment in parsed form
     */
    @ApiOperation(value = "Uploads a document referent to the customer address", notes = "The attachment must be named 'file'", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Address received successfully, must check the returned answer string in order to determinate if this is a valid response for the business"),
            @ApiResponse(code = 422, message = "The address document could not be parsed")
    })
    @Deprecated
    @RequestMapping(value = "/uploadParsed/{id}", method = RequestMethod.POST, consumes = "multipart/form-data")
    public ResponseEntity<String> addAddressDocumentAndParse(@RequestPart("file") MultipartFile file, @PathVariable Long id) {
    	log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".addAddressDocumentAndParse ");
        ResponseEntity<String> responseEntity = addAddressDocument(file, id);
        if (responseEntity.getStatusCode() == HttpStatus.UNPROCESSABLE_ENTITY) {
            return responseEntity;
        } else {
            String jsonResponse = responseEntity.getBody();
            JSONObject jsonObject = new JSONObject(jsonResponse);
            AddressDetailDTO addressDetailDTO = getDetailPreloaded(id);
            if (addressDetailDTO != null) {

            } else {
                String direUno = getDireUno(id);
                addressDetailDTO = DireParseUtil.parseDetail(direUno);

            }
            if (addressDetailDTO == null) {
                addressDetailDTO = new AddressDetailDTO();
            }
            jsonObject.put("country", addressDetailDTO.getCountry());
            jsonObject.put("extNumber", addressDetailDTO.getExtNumber());
            jsonObject.put("intNumber", addressDetailDTO.getIntNumber());
            jsonObject.put("locality", addressDetailDTO.getLocality());
            jsonObject.put("municipio", addressDetailDTO.getMunicipio());
            jsonObject.put("state", addressDetailDTO.getState());
            jsonObject.put("stret", addressDetailDTO.getStreet());
            jsonObject.put("suburb", addressDetailDTO.getSuburb());
            jsonObject.put("zipCode", addressDetailDTO.getZipCode());
            return new ResponseEntity<>(jsonObject.toString(), HttpStatus.OK);
        }
    }

    /**
     * Downloads the image of the address related to the request
     *
     * @param dto - The request
     * @return - The image related if any
     */
    @ApiOperation(value = "Downloads the address document related to the request json object", notes = "The request must be a valid json formed with the fields: {'personalIdentificationNumber','id','isAnverso'}", response = byte[].class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The document is found"),
            @ApiResponse(code = 404, message = "No document related to the given request"),
            @ApiResponse(code = 500, message = "Error from the server")
    })
    @RequestMapping(value = "/download", method = RequestMethod.POST)
    public ResponseEntity<byte[]> getImageFromReference(@RequestBody DocumentPictureRequestDTO dto) {
    	log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".getImageFromReference ");
        byte[] image = null;
        try {
            image = addressService.findPicture(dto);
            if (image == null) {
                return new ResponseEntity<>(image, HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(image, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error finding image for reference: {} with message: {}", dto, e.getMessage());
            return new ResponseEntity<>(image, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Finds the non parsed address
     *
     * @param id - The identifier of the db
     * @return
     */
    private String getDireUno(Long id) {
    	log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".getDireUno ");
        BidClieDireNestPK pk = new BidClieDireNestPK();
        pk.setIdClie(id);
        pk.setIdDire(id);
        try {
            BidClieDireNest direNest = direNestRepository.findOne(pk);
            String direUno = direNest.getDirNestUno();
            return direUno;
        } catch (Exception e) {
            log.error("Error finding dire for: {} with message: {}", id, e.getMessage());
            return null;
        }
    }

    private AddressDetailDTO getDetailPreloaded(Long id) {
    	log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".getDetailPreloaded ");
        try {
            AddressDetailDTO addressDetailDTO = new AddressDetailDTO();
            BidClieDireCont direCont = null;
            direCont = direContRepository.findTopByIdClieAndIdEsta(id, 1);
            addressDetailDTO.setLocality(direCont.getCol());
            addressDetailDTO.setState(direCont.getEsta());
            addressDetailDTO.setZipCode(direCont.getCp());
            addressDetailDTO.setMunicipio(direCont.getMuni());
            addressDetailDTO.setStreet(direCont.getDomi());
            return addressDetailDTO;
        } catch (Exception e) {
            log.error("Error finding pre loaded address for :{} with message: {}", id, e.getMessage());
            return null;
        }
    }

    /**
     * Updates record on db
     *
     * @param dto
     * @param id
     */
    private void updateDire(AddressDetailDTO dto, Long id) {
    	log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".updateDire ");
        BidClieDirePK pk = new BidClieDirePK();
        pk.setIdClie(id);
        pk.setIdDire(id);
        BidClieDire dire = bidDireRepository.findOne(pk);
        dire.setUsrOpeModi(dto.getUsername());
        if (dire == null) {
            dire = new BidClieDire();
            dire.setIdClie(id);
            dire.setIdDire(id);
            dire.setFchCrea(new Timestamp(System.currentTimeMillis()));
            dire.setUsrCrea("add-api");
            dire.setIdEsta(1);
            dire.setIdTipo(3);
        } else {
            dire.setFchModi(new Timestamp(System.currentTimeMillis()));
            dire.setUsrModi("add-api");
        }
        dire.setCall(dto.getStreet());
        dire.setCol(dto.getLocality());
        if (dto.getZipCode() != null && dto.getZipCode().length() > 5) {
            dire.setCp(dto.getZipCode().substring(0, 5));
        } else {
            dire.setCp(dto.getZipCode());
        }
        dire.setCp(dto.getZipCode());
        dire.setNoExt(dto.getExtNumber());
        dire.setNoInt(dto.getIntNumber());
        bidDireRepository.save(dire);

    }

}
